import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';

import { PlacesService } from './places.service';
import { Place } from './place.model';

@Controller('places')
export class PlacesController {
  constructor(private readonly placesService: PlacesService) {}

  @Post()
  addPlace(@Body('placeData')
  requestData: {
    title: string;
    description: string;
    imageUrl: string;
    price: number;
    availableFrom: Date;
    availableUntil: Date;
    userId: string;
  }) {
    const newPlace = this.placesService.appendPlace({ ...requestData });
    return newPlace;
  }

  @Get()
  getAllPlaces() {
    return this.placesService.getPlaces();
  }

  @Get(':id')
  getOnePlace(@Param('id') placeId: string) {
    return this.placesService.getSinglePlace(placeId);
  }

  @Patch(':id')
  updatePlace(
    @Param('id') placeId: string,
    @Body('placeData')
    requestData: {
      title: string;
      description: string;
      price: number;
      availableFrom: Date;
      availableUntil: Date;
    },
  ) {
    return this.placesService.updatePlace(placeId, { ...requestData });
  }

  @Delete(':id')
  removePlace(@Param('id') placeId: string) {
    this.placesService.deletePlace(placeId);
    return null;
  }
}
