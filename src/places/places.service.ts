import { Injectable, NotFoundException } from '@nestjs/common';

import { Place } from './place.model';

@Injectable()
export class PlacesService {
  private places: Place[] = [
    new Place(
      'p1',
      'Sotto la Mole in due',
      'Romantic under-the-roof apartment in front of the Mole Antonelliana of Torino',
      'https://s-ec.bstatic.com/images/hotel/max1280x900/956/95642025.jpg',
      80,
      new Date('2019-01-01'),
      new Date('2019-12-31'),
      'abc',
    ),
    new Place(
      'p2',
      'Junto a Nuestra Señora del Pilar',
      "Cosy apartment in Zaragoza's old town with a view on the Basilica",
      'https://res.cloudinary.com/lastminute/image/upload/c_scale,w_630/q_auto/v1534729489/zw5un06lmznljdvpgfad.jpg',
      70,
      new Date('2019-01-01'),
      new Date('2019-12-31'),
      'xyz',
    ),
    new Place(
      'p3',
      'Am Zürisee',
      'Beautiful and luminous apartment on the shore of lake Zurich',
      'https://media-cdn.tripadvisor.com/media/photo-s/10/06/36/f2/lake-view-executive-room.jpg',
      200,
      new Date('2019-01-01'),
      new Date('2019-12-31'),
      'abc',
    ),
  ];

  appendPlace(placeData: {
    title: string;
    description: string;
    imageUrl: string;
    price: number;
    availableFrom: Date;
    availableUntil: Date;
    userId: string;
  }) {
    const placeId = Math.random().toString();
    const newPlace = new Place(
      placeId,
      placeData.title,
      placeData.description,
      placeData.imageUrl,
      placeData.price,
      placeData.availableFrom,
      placeData.availableUntil,
      placeData.userId,
    );
    this.places.push(newPlace);
    return newPlace;
  }

  getPlaces() {
    return [...this.places];
  }

  getSinglePlace(placeId: string) {
    const place = this.findPlaceIndex(placeId)[0];
    return place;
  }

  updatePlace(
    placeId: string,
    updatedData: {
      title: string;
      description: string;
      price: number;
      availableFrom: Date;
      availableUntil: Date;
    },
  ) {
    const [place, index] = this.findPlaceIndex(placeId);
    const updatedPlace = new Place(
      place.id,
      updatedData.title,
      updatedData.description,
      place.imageUrl,
      updatedData.price,
      updatedData.availableFrom,
      updatedData.availableUntil,
      place.userId,
    );
    this.places[index] = { ...updatedPlace };
    return this.places[index];
  }

  deletePlace(placeId: string) {
    const index = this.findPlaceIndex(placeId)[1];
    this.places.splice(index, 1);
  }

  private findPlaceIndex(placeId: string): [Place, number] {
    const index = this.places.findIndex(place => placeId === place.id);
    const place = this.places[index];
    if (!place) {
      throw new NotFoundException('Could not find place'); // throws a 404 not found error
    }
    return [place, index];
  }
}
